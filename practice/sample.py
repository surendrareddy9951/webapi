from fastapi import FastAPI
from sqlalchemy import select
from pydantic import BaseModel
from hashpass import DBSession,Items,Users
import json
import os


app=FastAPI()
data =[]

class users(BaseModel):
    user_username: str
    user_password: str
    user_email: str


        
    
@app.on_event("startup")
def load_data():
    file_to_load="login.json"
    if os.path.exists(file_to_load):
        with open("login.json",'r') as file:
            global data 
            data= json.load(file)

@app.post("/posts/login/{username}")
def login(username:str):
    for entry in data:
        if entry["username"]==username:
            
            return {
                "message":"login successfully",
                "post":entry
            }
    return {
        "message":"user not found"
    }



@app.on_event("shutdown")
def save_data():
    with open("login.json",'w') as file:
        json.dump(data,file,indent=4)

