from fastapi import FastAPI
from pydantic import BaseModel
import os
import json

app=FastAPI()

data=[]

class Post(BaseModel):
    id: str
    description: str
    content: str



@app.post("/post/add")
def add_post(post:Post):
    data.append(post.dict())
    return {
        "message":"successfully added post to database",
        "post_added":{
            "id":post.id,
            "description":post.description,
            "content":post.content
        }
    }

@app.get("/posts")
def get_all_posts():
    return{
        "results":data
    }

@app.delete("/posts/delete/{post_id}")
def delete_post(post_id:str):
    for entry in data:
        if entry["id"]==post_id:
            data.remove(entry)
            return {
                "message":"post removed successfully",
                "post":entry
            }
    return {
        "message":"post not found"
    }

@app.on_event("startup")
def load_data():
    file_to_load="data.json"
    if os.path.exists(file_to_load):
        with open("data.json",'r') as file:
            global data 
            data= json.load(file)



@app.on_event("shutdown")
def save_data():
    with open("data.json",'w') as file:
        json.dump(data,file,indent=4)