from importlib.metadata import metadata
import string
from numpy import integer
from sqlalchemy import Float, column, create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.future import Engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import String,Column, Integer, ForeignKey
import requests




engine: Engine=create_engine("sqlite+pysqlite:///crypto.db")
session=Session(engine)

Base=declarative_base()



class Responses(Base):
    __tablename__="responses"
    row_no : int = Column(Integer,primary_key=True)
    ids = Column(String(64),unique=False)
    curriences  = Column(String(64))
    # curent_value : float = Column(Float)
    price_change = Column(Float)
    price_change_in_1hr = Column(Float)
    price_change_in_24hr= Column(Float)
    price_change_in_7d= Column(Float)

    def present_price(self):
        res=requests.get("https://api.coingecko.com/api/v3/coins",params={"id":self.ids}).json()
        
        self.current_price_change=res[0]['market_data']['current_price'][self.curriences]
        self.price_change_in_1hr=res[0]['market_data']['price_change_percentage_1h_in_currency'][self.curriences]
        self.price_change_in_24hr=res[0]['market_data']['price_change_24h_in_currency'][self.curriences]
        
        
Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)
