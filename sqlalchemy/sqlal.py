import models,schemas, database
from models import Users
from multiprocessing import connection
from fastapi import FastAPI, Cookie, Response
# from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.ext.asyncio import AsyncSession
#import asyncio
from sqlalchemy.orm import session
from asyncio import current_task
#from sqlalchemy.ext.asyncio import create_async_engine
# from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.asyncio import async_scoped_session
# from sqlalchemy.ext.asyncio import AsyncSession, AsyncEngine
from sqlalchemy import Column, Integer, String, Index, create_engine, ForeignKey
from sqlalchemy import select
from pydantic import BaseModel
from sqlalchemy.orm import validates, Session
from urllib.parse import urlparse
import re
import os
import hashlib
# from sqlalchemy import ForeignKey, create_engine,String,Integer,column
from sqlalchemy.future import Engine
from database import SessionLocal
from database import Base

# from sqlalchemy.models import Users



# models.Base.metadata.create_all(bind=Engine)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

def save(self, session: Session):
        session.add(self)
        session.commit()

# class User(BaseModel):
#     username: str
#     password: str
#     email: str | None



def main():
    Engine.begin()
    connection.run_sync(Base.metadata.drop_all)
    connection.run_sync(Base.metadata.create_all)


    user1 = Users(username="surendra", email="surendra@something.com")
    user1.hash_password("surendra@123")

    
    session.begin()
    session.add(user1)
    session.commit()


    sql_stmt = select(Users).where(Users.username==user1.username)
    print(sql_stmt)
    results = session.stream_scalars(sql_stmt)
    user =  results.first()
    if user.authenticate("surendra@123"):
        print("user authenticated!")





@app.get("/")
def hello(login_token: str | None = Cookie(None)):
    return {
        "message": "Visit '/docs' ",
        "login_token": login_token
    }


@app.post("/signup")
def signup(user: users):
    for entry in users:
        if entry.username == user.username:
           
            return {
                "status_code": "0",
                "message": f"User with {user.username} already exists!"
            }   
    new_user = Users(username=user.username, email=user.email)
    new_user.gen_hash(user.password)
    users.append(new_user)
    db_user=session.query(Users).filter(user.username==Users.username)
    if db_user==None:
        user=Users(**user.dict())
        user.gen_hash(password)
        session.add(user)
        session.commit()
    return {
        "status_code" : "1",
        "message": "user added successfully"
    }


@app.post("/login")
def login(user: User, response: Response):
    for entry in users:
        if entry.username == user.username:
            # check_user = Users.load_from_dict(entry)
            if entry.verify(user.password):
                response.set_cookie(key="login_token", value=entry.token)
                return {
                    "message": "login successful! user authenticated!"
                }
            else:
                return {
                    "message": "wrong password. user verification failed"
                }
    return {
        "message": f"user with {user.username} not found in database"
    }




@app.post("/logut")
def logut(user: User, response: Response):
    for entry in users:
        if entry.username == user.username:
            
            if entry.verify(user.password):
                response.delete_cookie(key="login_token")
                return {
                    "message": "logut successful! "
                }
            else:
                return {
                    "message": "wrong password. user verification failed"
                }
    return {
        "message": f"user with {user.username} not found in database"
    }

if __name__ == "__main__":
    main()