from sqlalchemy import Column, Integer, String, Index, create_engine, ForeignKey
from sqlalchemy.orm import relationship
from .database import Base
from pydantic import BaseModel
import os
import hashlib


class Users(Base):
    __tablename__ = "suri"
    # id: int = Column("id", Integer, primary_key=True)
    username: str = Column("username", String(128),primary_key=True, unique=True, nullable=False)
    hash_algorithm: str = Column("hash_algorithm", String(256))
    no_of_iterations: int = Column("no_of_iterations", Integer)
    salt: str = Column("salt", String(64))
    hashed_password: str = Column("hashed_password", String(128))
    email: str = Column("email", String(512))

    def hash_password(self, password: str):
        self.salt = os.urandom(64).hex()
        self.hash_algorithm = "pbkdf2-hmac-sha512"
        self.no_of_iterations = 100000
        self.hashed_password = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations).hex()

    def gen_hash(self, password: str):
        self.salt = os.urandom(64).hex()
        hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000)
        self.hashed_password = hash.hex()
    

    def authenticate(self, password: str):
        if self.hash_algorithm == "pbkdf2-hmac-sha512":
            new_hash = hashlib.pbkdf2_hmac(self.hash_algorithm.split("-")[-1], password.encode("utf-8"), self.salt.encode("utf-8"), self.no_of_iterations)
            if new_hash == self.hashed_password:
                return True
            else:
                return False

    def verify(self, password: str):
        new_hash = hashlib.pbkdf2_hmac("sha512", password.encode("utf-8"), self.salt.encode("utf-8"), 100000)
        if new_hash.hex() == self.hashed_password:
            self.token = os.urandom(64).hex()