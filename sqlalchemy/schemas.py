
from pydantic import BaseModel

class User(BaseModel):
    username: str
    password: str
    email: str | None

class posta(BaseModel):
    title:str
    description:str
    content:str
    author:str
