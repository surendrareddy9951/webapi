from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
from sqlalchemy.future import Engine
from sqlalchemy.ext.declarative import declarative_base


engine: Engine=create_engine("sqlite+pysqlite:///main.db")
session=Session(engine)

SessionLocal = sessionmaker(auticommit=False, autiflush=False, bind=engine)
Base=declarative_base